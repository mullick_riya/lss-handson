# LSS-HandsON
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/shadaba%2Flss-handson/master)


This has set of Jupyter notebooks which guide you through basics of Large Scale Struture Analysis. This has an inbuilt correlation function code as well.

Dependencies:
 - python
  - numpy==1.13.1
  - pip:
    - pandas==0.20.3
    - matplotlib==2.0.2
    - fitsio==0.9.11
    - Cython==0.26

It also need correlation function code which should be installed after the installing all the above dependencies. To install Correlation function code type use following command:
source postBuild

You can also directly access these notebook online with interactive by clicking on the binder link above.a
